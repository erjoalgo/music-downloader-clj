(defproject music-downloader-clj "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [compojure "1.4.0"]
                 [ring "1.5.0"]
                 [ring/ring-defaults "0.1.5"]
                 ;[ring/ring-defaults "1.5.0"]
                 ;;[ring/ring-jetty-adapter "1.5.0"]
                 [ring/ring-jetty-adapter "1.5.0"]
                 [org.clojure/clojure "1.8.0"]
                 [clj-soup/clojure-soup "0.1.3"]
                 [hiccup "1.0.5"]
                 [org.clojure/tools.logging "0.3.1"]
                 [ring-basic-authentication "1.0.5"]
                 ]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler music-downloader-clj.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
