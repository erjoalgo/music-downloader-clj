(defn maybe-add-missing-protocol [uri-str]
  (if (clojure.string/starts-with? uri-str "//")
    (str "http:" uri-str)
    uri-str))
