(ns music-downloader-clj.handler
  (:require
                                        ;[compojure.core :refer :all]
   [compojure.core :refer [routes GET POST]]
   [compojure.route :as route]
   [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
   [ring.middleware.stacktrace :refer [wrap-stacktrace]]
   [ring.middleware.params :refer [wrap-params]]
   [hiccup.core :refer [html]]
   [jsoup.soup :refer [get! $ attr select]]
   [ring.adapter.jetty :refer [run-jetty]]
   [ring.util.io :refer [piped-input-stream]]
   [clojure.tools.logging :as log]
   [ring.middleware.basic-authentication :refer [wrap-basic-authentication]]
   [clojure.java.io :refer [file resource]]
   [clojure.java.shell :refer [sh]]
   [clojure.string :refer [split]]
   ))




;;https://www.youtube.com/results?search_query=are+you+going+to+scarborough+fair
(def youtube-base-url "https://www.youtube.com")
(def videos-top-directory "/tmp")
(def htpasswd-fn (try (->
                       "basic-auths.htpasswd"
                       clojure.java.io/resource
                       .getFile)
                      (catch Exception ex)))

(defn fetch-video-page-by-query [search-query]
  ;;TODO how to encode query to uri
  (->>
   (clojure.string/replace search-query " " "+")
   (str youtube-base-url "/results?search_query=")
   get!))

(defn scrape-html-to-map [key-to-selectors html-el]
  "given a list of tripes: (key css-selector attr-selector)
   return a map of keys to attr values"
  (reduce (fn [cum [key class-selector attr-selector]]
            (conj cum {key (->> html-el
                                (select class-selector)
                                (attr attr-selector)
                                first)}))
          {}
          key-to-selectors))

(defn extract-videos [search-page-html]
  (->> ($ search-page-html ".yt-uix-tile")
       (map (partial
             scrape-html-to-map
             [[:thumb-url ".yt-lockup-thumbnail .yt-thumb-simple img" "src"]
              [:url ".yt-lockup-title a" "abs:href"]
              [:title ".yt-lockup-title a" "title"]]))))

(defn find-videos [search-query]
  (-> search-query
      fetch-video-page-by-query
      extract-videos))

(defn video-url-to-id [url]
  (->> url
       (re-matches #".*?watch[?]v=(.*)" )
       second))

(defn video-id-to-url [id]
  (str "https://www.youtube.com/watch?v=" id))

(def output-format "mp3")
(defn video-url-to-fn [url]
  (-> url
      video-url-to-id
      (str "." output-format)))

(defn video-to-fn [video]
  (log/debug "original title: " (:title video))
  (let [sanitized-title
        (-> (:title video)
            (clojure.string/replace
             #"[ -]+" "-")
            (clojure.string/replace
             #"[^a-zA-Z-]" ""))
        id (video-url-to-id (:url video))]
    (log/debug "sanitized title: " sanitized-title)
    (format "%s-%s.%s"
            sanitized-title
            id
            output-format)))

(defn fn-to-url [fn]
  (->> fn
       (re-matches #".*(.{11})[.][a-z0-9]+$")
       second
       video-id-to-url))

(defn video-id-to-thumb-url [id & {:keys [size-index]
                                   :or {:size-index 0}}]
  (format "http://img.youtube.com/vi/%s/%d.jpg" id 0))

(defn download-video [url dir]
  (let [fn (video-url-to-fn url)
        fn-abs (file dir fn)]
    (when-not (.exists fn-abs)
      (let [args ["youtube-dl" "--id" "--extract-audio"
                  (str "--audio-format=" output-format) url
                  :dir dir]
            _ (log/infof "youtube command: %s" args)
            proc (apply sh args)]
        (when-not (-> proc :exit (= 0))
          (throw (Exception. (format "youtube-dl failed: %s" proc))))))
    (.toString fn-abs)))

(defn htpasswd-users-to-passwords [htpasswd-fn]
  (->> (-> htpasswd-fn
           slurp
           (split #"\n"))
       (map #(split % #":"))
       flatten
       (apply hash-map)))

(defn htpasswd-= [hashed plain]
  ;;TODO assuming hashed is plaintext, use library to compare
  (= hashed plain))

(defn authenticated? [htpasswd-fn user pass]
  (or (not htpasswd-fn)
      (-> (htpasswd-users-to-passwords htpasswd-fn)
      (get user)
      (htpasswd-= pass))))

(def app
  (wrap-stacktrace
   (routes
    (GET "/" [] (-> [:body
                     {:style
                      (str
                       "background-image:"
                       "url(\"./resources/public/lisp-lizard.png\");"
                       "background-position: right;"
                                        ;"background-position: right bottom;"
                       "background-repeat:no-repeat;"
                       "background-size: 15%;")}
                     [:form {:action "/by-lyrics" :method "GET"}
                      "Search by lyrics: " [:br]
                      [:input {:type "text"
                               :name "lyrics"
                               :style "width:300px"
                               :autofocus true}] [:br]
                      [:button {:type "submit" :style "width:300px"}
                       "Search videos"] [:br]
                      [:button {:type "submit" :style "width:300px"
                                :name "lucky" :value "true"}
                       "I'm feeling lucky"] [:br]
                      ]]
                    html))

    (GET "/by-lyrics" []
         (wrap-params
          (fn [req]
            (let [lyrics (get (:query-params req) "lyrics")
                  lucky (get (:query-params req) "lucky")]
              (let [videos (find-videos lyrics)]
                (if (empty? videos)
                  (str "No videos found for query: "
                       lyrics)
                  (if (= "true" lucky)
                    ;;301 or 302? should be "temporary"
                    (ring.util.response/redirect
                     (str "/fetch?" (-> videos first :url)))
                    (->> videos
                         (map (fn [video-map]
                                (let [url (str "/fetch/"
                                               (video-to-fn video-map))]
                                [:tr
                                 [:td [:a {:href url}
                                       [:img {:src
                                             (str "/proxy?"
                                                  (-> video-map
                                                      :url
                                                      video-url-to-id
                                                      video-id-to-thumb-url))}]]]
                                 [:td [:a {:href url}
                                       (:title video-map)]]])))
                         (apply vector :table)
                         html))))))))

    (GET "/fetch/:video-fn" [video-fn]
         (-> (fn [req]
               (log/infof "fetching %s\n" video-fn)
               (->
                (fn-to-url video-fn)
                (download-video videos-top-directory)
                clojure.java.io/input-stream
                ring.util.response/response
                (ring.util.response/header "Content-Type"
                                           "audio/mp3")))
             (wrap-basic-authentication
              (partial authenticated? htpasswd-fn))))

    (GET "/proxy" []
         (fn [req]
           (log/debugf "proxying %s\n" (:query-string req))
           (->
            req
            :query-string
            clojure.java.io/input-stream)))

    (route/resources "/resources/public")

    (route/not-found "Not Found"))))

'(do;;repl testing
                                        ;(use 'music-downloader-clj.handler)
                                        ;(use 'jsoup.soup)
  (if (and (resolve `test-server)
           (var-get (resolve `test-server)))
    (.stop (var-get (resolve `test-server))))

  (def test-server
    (ring.adapter.jetty/run-jetty app {:port 1239 :join? false})))
